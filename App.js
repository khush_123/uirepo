import React, {Component} from 'react';
import { StyleSheet, Text, View} from 'react-native';
import Header from './src/Header';
import Page from './src/Page';

export default class App extends Component {
  render(){
    return(
      <View style={styles.container}>
       <Header />
       <Page />
      </View>

    );
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});