import React, { Component } from 'react'
import { Alert, StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

export default class Page extends Component{
  onPressButton() {  
        Alert.alert('open login form!')  
    }  
  render(){
    return(
    <View style={styles.container}>
      <View style= {styles.box}>
        <View style={styles.inner}>
        <Text style={styles.home}>Welcome to Home</Text>
           <View style={styles.buttonContainer}>  
                    <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5}>
    <Image
     source={require('./Image/log.png')}
     style={styles.ImageIconStyle}
    />
    <View style={styles.SeparatorLine} />
    
</TouchableOpacity> 
        </View>
      </View>
    </View>
    </View>
    );
  }
}

const styles =StyleSheet.create({
  container: {
    width: '100%',
    height: '85%',
    padding: 5,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  box: {
    width: '100%',
    height: '100%',
    padding: 2
  },
  inner: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
   buttonContainer: {  
      margin: 90,
      backgroundColor: 'white',
      color: '#3A59FF',
      width: "65%",
      borderRadius:1,
      textAlign: 'center',
      fontWeight: 'bold',
      marginLeft: '18%',
      padding: "1%",
      fontSize:  70,
      marginTop: '50%'
          
    },
    home: {
      marginTop: -60,
      fontSize: 40
    },
    ImageIconStyle: {
    padding: 10,
    margin: 5,
    marginLeft: '30%',
    height: 60,
    width: 150,
    resizeMode: 'stretch'
   },
});